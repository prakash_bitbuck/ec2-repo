provider "aws" {
    region = "us-east-1"
    secret_key =  var.secret_key
    access_key = var.access_key
}

resource "aws_security_group" "allow_ssh" {
  name        = var.sg_details["ssh"]["name"]
  description = "Allow SSH inbound traffic"
  ingress {
    description      = "ssh from VPC"
    from_port        = var.sg_details["ssh"]["port"]
    to_port          = var.sg_details["ssh"]["port"]
    protocol         = var.sg_details["ssh"]["proto"]
    cidr_blocks      = [var.sg_details["ssh"]["CID"]]
  }
  ingress {
    description      = "http port"
    from_port        = var.sg_details["http"]["port"]
    to_port          = var.sg_details["http"]["port"]
    protocol         = var.sg_details["http"]["proto"]
    cidr_blocks      = [var.sg_details["http"]["CID"]]
  }
  egress {
    description      = "all traffic"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "my_jenkins" {
    ami = var.image_id
    instance_type = var.instance_size["prod"]
    subnet_id = "subnet-0b1e151c6683d6dd1"
    key_name =  "prakash_key"
    vpc_security_group_ids = [aws_security_group.allow_ssh.id]
    tags = {Name = "my_jenkins" }
    

provisioner "remote-exec" {
inline = ["echo 'testing ssh'"]
  connection {
    type     = "ssh"
    user     = "ec2-user"
    private_key = "${file("/var/lib/jenkins/id_rsa")}"
    host     = aws_instance.my_jenkins.public_ip
  }
}


provisioner "remote-exec" {
inline = ["echo 'testing jenkins' >>/tmp/prakash.txt"]
  connection {
    type     = "ssh"
    user     = "ec2-user"
    private_key = "${file("/var/lib/jenkins/id_rsa")}"
    host     = aws_instance.my_jenkins.public_ip
  }
}

}