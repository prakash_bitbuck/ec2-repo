variable "access_key" {
    type = string
  
}
variable "secret_key" {
    type = string
}

variable "region" {
    type = string
}

variable "image_id" {
  type= string
}

variable "sg_details" {
    type = map
  
}

variable "instance_size" {
    type = map
  
}