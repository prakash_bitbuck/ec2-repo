provider "aws" {
    region = "us-east-1"
}
  
module "ec2-creation" {
    source = "../module/ec2-module/"
    region = "us-east-1"
    image_id = "ami-04ad2567c9e3d7893"
    sg_details = {
    "ssh": { "name" : "ssh_SG", "port" : "22", "proto" : "tcp", "CID" = "0.0.0.0/0"}
    "http" : { "name" : "http_SG", "port" : "80", "proto" : "tcp", "CID" = "0.0.0.0/0"}
    "jenkins" : { "name" : "jenkins_port", "port" : "8080", "proto" : "tcp", "CID" = "0.0.0.0/0"}
    }
    instance_size = {
    "dev" : "t2.micro"
    "uat" : "t2.micro"
    "prod" : "t2.micro"
    }
    secret_key =  var.secret_key
    access_key = var.access_key
}